#!/usr/bin/env bash
set -euo pipefail
[ -d drupal10 ] || mkdir drupal10
cd drupal10
ddev config --docroot=web --project-type=drupal10 --project-name agaric-drupal10 --create-docroot
ddev start
ddev composer create -n "drupal/recommended-project"
ddev composer require -n "drush/drush"
ddev drush site:install -y --account-name=admin --account-pass=admin
ddev composer require -n "drupal/devel:^5.1" "drupal/address:^2.0@beta" "drupal/migrate_plus:^6.0" "drupal/migrate_tools:^6.0" "drupal/migrate_upgrade:^4.0" "drupal/paragraphs:^1.16"